package com.example.prubajira

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.prubajira.ui.theme.PrubajiraTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PrubajiraTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting()
                }
            }
        }
    }
}

@Composable
fun Greeting() {
    var lisdat by remember {
        mutableStateOf(
            listOf<TablaState>(
                TablaState(
                    listOf(
                        ColumState(listOf(ElementState(), ElementState(), ElementState(5555))),
                        ColumState(),
                        ColumState(listOf(ElementState()))
                    ), "sprin 1"
                ), TablaState(
                    listOf(
                        ColumState(listOf(ElementState(), ElementState(), ElementState(5555))),
                        ColumState(),
                        ColumState(listOf(ElementState()))
                    ), "sprin 2"
                )
            )
        )
    }
    val scope = rememberCoroutineScope()
    var box by remember {
        mutableStateOf(-1)
    }
    var row by remember {
        mutableStateOf(-1)
    }

    val state = rememberReorderableLazyListState(canDragOverboxrow = {
        Log.e("cam", "$box $row")
        Pair(box, row)
    }, canDragOver = { from, to ->
        Log.e("hola cam", from.toString() + to.toString())
        true
    }, onMove = { from, to, onprees ->
        Log.e("move", from.toString() + to.toString())
        Log.e("move", "${row} to ${box}")
        scope.launch(Dispatchers.IO) {
            val (rowpress, boxpress) = onprees()
            if (boxpress != -1 && rowpress != -1) {
                val newlist = lisdat.toMutableList()
                val fila = newlist.getOrNull(rowpress)
                val columaant = fila?.child?.getOrNull(from.index)
                val columantlist = columaant?.child?.toMutableList()
                val columato = fila?.child?.getOrNull(to.index)
                val columatolis = columato?.child?.toMutableList()
                val boxcopia = columantlist?.getOrNull(rowpress)
                columantlist?.removeAt(boxpress)
                boxcopia?.let {
                    columatolis?.add(it)
                }
                columato?.child = columatolis
                columaant?.child = columantlist
                fila?.child?.toMutableList()?.apply {
                    removeAt(to.index)
                    columato?.let {
                        add(to.index, it)
                    }
                    if (from.index < this.size)
                        removeAt(from.index)
                    columaant?.let {
                        add(from.index, it)
                    }
                }
                lisdat = newlist.apply {
                    if (rowpress < this.size)
                        removeAt(rowpress)
                    else
                        removeAt(0)
                    fila?.let {
                        add(rowpress, fila)
                    }
                }
                if (columato?.child?.size != null)
                    box = columato.child?.size!! - 1
            }
        }

    })
    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(), verticalArrangement =
        Arrangement.spacedBy(20.dp)
    ) {
        itemsIndexed(lisdat) { inde, tabla ->
            Text(text = tabla.text)
            LazyRow(
                modifier = Modifier
                    .fillMaxWidth()
                    .reorderable(state) {
                        row = inde
                        Pair(inde, box)
                    },
                state = state.listState
            ) {
                itemsIndexed(tabla.child, { index, item ->
                    row = inde
                    index
                }) { columindex, colum ->
                    ReorderableItem(
                        state,
                        colum,
                        index = columindex,
                        modifier = Modifier.fillMaxSize(),
                    ) { isDragging, boxselct, rowselect ,mod->
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                    ) {
                        if (colum.child.isNullOrEmpty()) {
                            Box(
                                modifier = Modifier
                                    .padding(5.dp)
                                    .height(500.dp)
                            ) {
                                Text(text = "aqui toy")
                            }
                        }
                        (colum.child)?.forEachIndexed { columinde, eletmen ->
                                var modificadorbox = Modifier
                                    .padding(5.dp)
                                if (columinde == box && inde == row && isDragging) {
                                    val elevation =
                                        animateDpAsState(if (isDragging) 16.dp else 0.dp)
                                    modificadorbox = mod.then(
                                        Modifier.padding(5.dp).shadow(elevation.value)
                                    )
                                }
                                modificadorbox = modificadorbox.then(
                                    Modifier
                                        .height(250.dp)
                                        .width(50.dp)
                                        .background(
                                            color = Color(
                                                (1..255).random(),
                                                (1..255).random(), (1..255).random()
                                            )
                                        )
                                        .detectReorderAfterLongPress(state) {
                                            box = columinde
                                            row = inde
                                            columinde
                                        })
                                Box(
                                    modifier = modificadorbox
                                ) {
                                    Text(text = "${eletmen.id}")
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}


data class TablaState(
    var child: List<ColumState>,
    val text: String,
)

data class ColumState(var child: List<ElementState>? = listOf())

data class ElementState(
    val id: Int = (1..255).random(),
)


@Composable
fun VerticalReorderList() {
    val data = remember { mutableStateOf(List(100) { "Item $it" }) }
    val state = rememberReorderableLazyListState(onMove = { from, to, opn ->
        data.value = data.value.toMutableList().apply {
            add(to.index, removeAt(from.index))
        }
    })
    LazyColumn(
        state = state.listState,
        modifier = Modifier
            .reorderable(state)
            .detectReorderAfterLongPress(state)
    ) {
        items(data.value, { it }) { item ->
            ReorderableItem(state, key = item) { isDragging, boxsele, ro,mod ->
                val elevation = animateDpAsState(if (isDragging) 16.dp else 0.dp)
                Column(
                    modifier = Modifier
                        .shadow(elevation.value)
                        .background(
                            Color(
                                (1..255).random(),
                                (1..255).random(), (1..255).random()
                            )
                        )
                ) {
                    Text(item)
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    PrubajiraTheme {
        Greeting()
    }
}